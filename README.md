# Examen outils informatiques / Formation de base en informatique

**Note**: lorsque la mention "ajouter ce fichier au dépôt" ou "ajouter les changements au dépôt" apparait il s'agit de créer un commit contenant le fichier ou les modifications apportées. Le commit devra porter comme nom "*SECT*_Q*X*" pour la question *X* de la section *SECT* (par exemple GIT_Q2). Si une question n'a pas de numéro simplement indiquer la section (e.g. SSH).

## GIT

1. *Forker* ce dépôt pour obtenir votre propre version sur gitlab.
2. *Cloner* votre version du dépôt (attention à bien utiliser la version SSH et PAS https).
3. Créer une nouvelle branche portant votre nom.
4. Pour le reste de l'examen vous travaillerez sur cette nouvelle branche uniquement, sauf pour les toutes dernières questions (c.f. question *merge*).
5. Taper la commande `./script.sh` pour lancer le programme correspondant. Ce programme a créé un fichier, ajouter ce fichier au dépôt.

## Shell / Bash

1. Supprimer **TOUS** les fichiers se terminant par *.txt dans le dossier `txt`. Ajouter les changements au dépôt.
2. Supprimer le fichier `toerease` du dossier `damn`. Ajouter les changements au dépôt.
3. Le fichier `data.csv` contient des données au format colonne. Visualiser les 5 premières lignes de ce fichier et les stocker dans un fichier nommé `data-excerpt.csv`. Ajouter ce fichier au dépôt.
4. En combinant les commandes vues en cours ainsi que la commande `cut` (c.f. man), trouver une commande (i.e. une ligne) permettant d'obtenir *uniquement* le pourcentage d'utilisation de la lettre "X". Ecrire cette commande dans un fichier nommé `extract-line.sh` et ajouter ce fichier à la racine du dépôt.

## SSH

En se connectant sur la machine [10.25.10.53](10.25.10.53) par SSH copier le fichier distant `/home/chanel/toget` à la racine de votre dépôt local. Ajouter ce fichier au dépôt.

## Fichiers

En utilisant un éditeur hexadécimal, stocker dans un fichier nommé `data.bin` la valeur décimale 256 en *little-endian* sur deux octets. Ajouter ce fichier à la racine du dépôt.

## GIT

1. Effectuer un *merge* de la branche portant votre nom vers la branche master.
2. Monter le contenu local de votre dépôt vers votre *remote*.
3. Effectuer un *pull (ou merge) request* sur le dépôt originel qui aura pour titre *nom_prenom* où vous indiquerez votre nom et votre prénom.

# Soumettre l'évaluation

Se rendre sur moodle et soumettre l'adresse gitlab de votre dépôt dans le formulaire de l'évaluation.
